﻿import java.util.Scanner;
class Fahrkartenautomat
{
  public static void fahrkarten_druck(){
   System.out.printf("1  Einzelfahrschein Berlin AB 2,90"
   + "\n2  Einzelfahrschein Berlin BC  3,30"
   + "\n3  Einzelfahrschein Berlin ABC 3,60"
   + "\n4  Kurzstrecke 1,90"
   + "\n5  Tageskarte Berlin AB  8,60"
   + "\n6  Tageskarte Berlin BC  9,00"
   + "\n7  Tageskarte Berlin ABC 9,60"
   + "\n8  Kleingruppen-Tageskarte Berlin AB 23,50"
   + "\n9  Kleingruppen-Tageskarte Berlin BC 24,30"
   + "\n10 Kleingruppen-Tageskarte Berlin ABC  24,90\n\n");
  }
 
  public static double fahrkarten_wahl(int auswahl){
    String[] fahrkarten = new String[10];
    double[] preis = new double[10];
 
   fahrkarten[0] = "Einzelfahrschein Berlin AB";
   fahrkarten[1] = "Einzelfahrschein Berlin BC";
   fahrkarten[2] = "Einzelfahrschein Berlin ABC";
   fahrkarten[3] = "Kurzstrecke";
   fahrkarten[4] = "Tageskarte Berlin AB";
   fahrkarten[5] = "Tageskarte Berlin BC";
   fahrkarten[6] = "Tageskarte Berlin ABC";
   fahrkarten[7] = "Kleingruppen-Tageskarte Berlin AB";
   fahrkarten[8] = "Kleingruppen-Tageskarte Berlin BC";
   fahrkarten[9] = "Kleingruppen-Tageskarte Berlin ABC";
 
   preis[0] = 2.90;
   preis[1] = 3.30;
   preis[2] = 3.60;
   preis[3] = 1.90;
   preis[4] = 8.60;
   preis[5] = 9.00;
   preis[6] = 9.60;
   preis[7] = 23.50;
   preis[8] = 24.30;
   preis[9] = 24.90;
 
   System.out.printf("Sie haben sich für %s zum Preis von %.2f entschieden.\n\n", fahrkarten[auswahl - 1], preis[auswahl - 1]);
   return preis[auswahl - 1];
  }
 
  public static int anzahl_fahrkarten(){
    Scanner tastatur = new Scanner(System.in);
    System.out.print("Anzahl Fahrkarten: ");
   int anzFahrkarten = tastatur.nextInt();
   if (anzFahrkarten <= 10 && anzFahrkarten >= 1) {
      return anzFahrkarten;
   } else {
     System.out.println("Keine zulässige Anzahl Fahrkarten, bitte eine Anzahl zwischen 1 und 10 wählen.");
     return anzahl_fahrkarten();
   }
  }
  public static double fahrkarten_bestellung(){
   Scanner tastatur = new Scanner(System.in);
   int auswahl = 0;
   do {
     System.out.printf("\nWelche Fahrkarte wollen sie kaufen? (Auswahl von 1 bis 10)\n\n");
     fahrkarten_druck();
     auswahl = tastatur.nextInt();
   } while (auswahl < 1 || auswahl > 10);
   double zuzahlenderBetrag = fahrkarten_wahl(auswahl);
    int anzahlFahrkarten = anzahl_fahrkarten();
   return zuzahlenderBetrag * anzahlFahrkarten;
  }
  public static double bezahlen(double preis){
     Scanner tastatur = new Scanner(System.in);
     double eingezahlterGesamtbetrag = 0.0;
     while(eingezahlterGesamtbetrag < preis)
     {
       System.out.printf("Noch zu zahlen: %.2f%n", (preis - eingezahlterGesamtbetrag)); 
       System.out.print("Eingabe (mind. 5 	 bct, höchstens 2 Euro): ");
       double eingeworfeneMünze = tastatur.nextDouble();
       eingezahlterGesamtbetrag += eingeworfeneMünze;
     }
     return eingezahlterGesamtbetrag - preis;
  }
  public static void rückgeld(double rück){
      if(rück == 0.0)
    	  System.out.printf("Sie haben passend bezahlt und es wird kein Rückgeld ausgegeben.");
      else {
    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f%s%n ",rück, " EURO");
    	   System.out.printf("wird in folgenden Münzen ausgezahlt: ");
  }
    while (rück >= 2.0) {
    System.out.println("2 EUR");
     rück -= 2.0;
    }
     while(rück >= 1.0) // 1 EURO-Münzen
     {
     System.out.println("1 EUR");
     rück -= 1.0;
     }
     while(rück >= 0.5) // 50 CENT-Münzen
     {
     System.out.println("50 ct");
     rück -= 0.5;
     }
     while(rück >= 0.2) // 20 CENT-Münzen
     {
     System.out.println("20 ct");
     rück -= 0.2;
     }
     while(rück >= 0.1) // 10 CENT-Münzen
     {
     System.out.println("10 ct");
     rück -= 0.1;
     }
     while(rück >= 0.05)// 5 CENT-Münzen
     {
     System.out.println("5 ct");
     rück -= 0.1;
     }
  }
  public static void fahrkarten_ausgabe(){
    System.out.println("\nFahrschein/e werden ausgegeben");
    for (int i = 0; i < 8; i++){
      System.out.print("=");
      try {
        Thread.sleep(150);
      }
      catch (InterruptedException e) {
      e.printStackTrace();
      }
    }
    System.out.println("\n\n");
    System.out.println("\nVergessen Sie nicht, ihre/n Fahrschein/e\n"+"vor Fahrtantritt zu entwerten!\n"+"Wir wünschen Ihnen eine gute Fahrt.");
  }
  public static boolean more_tickets() {
    Scanner tastatur = new Scanner(System.in);
    String answer = "";
    System.out.printf("\nWollen Sie weitere Karten kaufen? (Y/N)");
    answer = tastatur.nextLine();
    switch (answer) {
      case "Y" :
       return true;
      case "N" :
       System.out.println("Vielen Dank für Ihren Einkauf und bis zum nächsten mal!");
       return false;
      default :
       System.out.println("Ungültige Eingabe!");
       return more_tickets();
    }
  }
 
  public static void main(String[] args)
  {
    boolean more = true;
    while (more) {
      rückgeld(bezahlen(fahrkarten_bestellung()));
      fahrkarten_ausgabe();
      more = more_tickets();
      }
  }
}
