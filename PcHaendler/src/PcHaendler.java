import java.util.Scanner;

public class PcHaendler {
	
	public static String liesString(String text) {
		Scanner tastatur = new Scanner(System.in);
		System.out.println(text);
		String artikel = tastatur.next();
		return artikel; 
	}
    public static int liesInt(String text) {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println(text);
    	int anzahl = tastatur.nextInt();
    	return anzahl;
    	
    }
    public static double liesdouble(String text) {
    	Scanner tastatur = new Scanner(System.in);
    	System.out.println(text);
		double listenpreis = tastatur.nextDouble();
		return listenpreis;
    }
   public static double berechnenNettogesamtpreis(int anzahl, double listenpreis ) {
	return anzahl * listenpreis;   
   }
   public static double berechnenBruttogesamtpreis(double berechnenNettogesamtpreis, double mwst) {
	   return berechnenNettogesamtpreis * (1 + mwst);
   }
   public static void rechnungsausgabe (String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst ) {
	   System.out.println("\tRechnung");
	   System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
	   System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.2f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
   }
   
		public static void main(String[] args) {
			final double  mwst = 0.19;
			String artikel = liesString("Was m�chten Sie bestellen?");
			int anzahl = liesInt("Geben Sie die Anzahl ein:");
			double listenpreis = liesdouble("Geben Sie den Listenpreis ein:");
			double nettogesamtpreis = berechnenNettogesamtpreis(anzahl, listenpreis);
			double gesamtpreis = berechnenBruttogesamtpreis(nettogesamtpreis, mwst);
			rechnungsausgabe(artikel, anzahl, nettogesamtpreis, gesamtpreis, mwst);
	}

}